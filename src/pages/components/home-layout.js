import React from 'react';

import '../../sass/styles.scss';

function HomeLayout(props) {
  return (
    <section className="HomeLayout">
      {props.children}
    </section>
  )
}

export default HomeLayout;