import React from 'react';

import logo from '../../../images/logo.png';
import '../../sass/styles.scss';

function Related(props) {
  return (
    <div className="Related">
      <img className="Logo" src={logo} width={80} height={80}/>
    </div>
  )
}

export default Related;